---
theme: gaia
class:
  - lead
  - invert
paginate: true
---

# Настройка окружения
## Хекслет: Rails-программист

---

# Установка Ruby

* ## [rvm](https://github.com/rbenv/rbenv)
* ## [rbenv](https://rvm.io/)
* ## [asdf](https://asdf-vm.com/#/core-manage-asdf) + [asdf-ruby](https://github.com/asdf-vm/asdf-ruby)

---

# Работа с библиотеками в Ruby

* ## [bundler](https://bundler.io/) - менеджер gem'ов в Ruby.

---

# Установка bundler

```ruby
  gem install bundler
```

---

# Работа с bundler. Создание Gemfile

```ruby
  bundle init
```

---

# Работа с bundler. Основные команды

```ruby
bundle install - установка пакетов

bundle update [gem] - обновление версии указанного пакета

bundle exec - исполнение команды в контексте текущих пакетов

bundle help - справка
```

---

# Утилита [rake](https://github.com/ruby/rake) - Ruby Make

* Используя окружение:

  ```ruby
    gem install rake
  ```

* Добавив в Gemfile:

  ```ruby
    gem 'rake'
  ```

---

# Работа с rake. Создание Rakefile

  ```bash
    $ls

    Rakefile task1.rb task2.rb
  ```

 ```ruby
    task default: %w[step1 step2]

    task :step1 do
      ruby "task1.rb"
    end

    task :step2 do
      ruby "task2.rb"
    end
  ```
---

# Работа с rake. Запуск

  ```bash
    $ rake
  ```

---

# Качество кода в Ruby

* ## [rubocop](https://github.com/rubocop/rubocop) - линтер для Ruby

---

# Установка rubocop

* Используя окружение:

  ```ruby
    gem install rubocop
  ```

* Добавив в Gemfile:

  ```ruby
    gem 'rubocop', require: false
  ```

---

# Работа с Rubocop. Конфигурация

```bash
  $ ls

 .rubocop.yml ruby.rb
```


```ruby
  $ cat .rubocop.yml

  ...

  Style/Encoding:
    Enabled: false

  Layout/LineLength:
    Max: 99

  ...
```

---

# Работа с Rubocop. Запуск

```ruby
$ rubocop
```

С автокоррецией:

```ruby
$ rubocop -A
```

С безопасной автокоррецией:

```ruby
$ rubocop -a
```

---

# Ruby стиль. Отступы

```ruby

# bad - four spaces
def some_method
    do_something
end

# good
def some_method
  do_something
end

```

---

# Ruby стиль. Выражения

```ruby

# bad
puts 'foo'; puts 'bar' # two expressions on the same line

# good
puts 'foo'
puts 'bar'

puts 'foo', 'bar' # this applies to puts in particular

```

---

# Ruby стиль. Пробелы

```ruby

# bad
sum=1+2
a,b=1,2
class FooError<StandardError;end

# good
sum = 1 + 2
a, b = 1, 2
class FooError < StandardError; end

```

---

# Ruby стиль. Правила

[Ruby Style Guide](https://rubystyle.guide/) - основной документ по стилистике написания кода в Ruby.


---

# Время для вопросов

=)
