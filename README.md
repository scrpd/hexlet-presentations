## Зависимости

* Node.js
* Make

## Установка генератора презентаций

```sh
make setup
```

## Генерация и запуск презентации

```sh
make presentation
# Любые изменения презентации presentation.md,
# будет автоматически менять presentation.html и перезагружать страницу

```

## Инструменты

* https://marpit.marp.app/markdown
* https://marpit.marp.app/directives
